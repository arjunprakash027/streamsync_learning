import streamsync as ss

# This is a placeholder to get you started or refresh your memory.
# Delete it or adapt it as necessary.
# Documentation is available at https://streamsync.cloud

# Shows in the log when the app starts
print("Hello world!")

# Its name starts with _, so this function won't be exposed
def _update_message(state):
    is_even = state["counter"] % 2 == 0
    message = ("+Even" if is_even else "-Odd")
    state["message"] = message

def decrement(state):
    print(state)
    state["counter"] -= 1
    _update_message(state)

def increment(state):
    state["counter"] += 1
    # Shows in the log when the event handler is run
    print(f"The counter has been incremented.")
    print(f"running running")
    _update_message(state)

def process_file(state, payload):

    uploaded_files = payload
    for i, uploaded_file in enumerate(uploaded_files):
        #print(i,uploaded_file)
        name = uploaded_file.get("name")
        file_data = uploaded_file.get("data")
        #print(name,file_data)
        with open(f"{name}-{i}.jpeg", "wb") as file_handle:
            file_handle.write(file_data)    
        state["updation_replies"]["img_upload_reply"] = "Image Uploaded Succussfully!!"
        
initial_state = ss.init_state({
    "my_app": {
        "title": "My App",
        "title2":"Side Bar 1"
    },
    "updation_replies":{
        "img_upload_reply":None
    },
    "_my_private_element": 1337,
    "message": None,
    "counter": 26,
})

_update_message(initial_state)